%if 0%{?fedora} || 0%{?rhel} >= 7
%global with_python3 1
%else
%global with_python3 0
%endif

%{?filter_provides_in: %filter_provides_in .*/h5py/.*\.so}
%{?filter_setup}

Summary:        A Python interface to the HDF5 library
Name:           h5py
Version:        2.7.1
Release:        1%{?dist}
Group:          Applications/Engineering
License:        BSD
URL:            http://www.h5py.org/
Source0:        https://pypi.python.org/packages/source/h/h5py/h5py-%{version}.tar.gz
# patch to use a system liblzf rather than bundled liblzf
Patch0:         h5py-system-lzf.patch
BuildRequires:  gcc
BuildRequires:  liblzf-devel
BuildRequires:  hdf5-devel >= 1.8.3
BuildRequires:  python2-devel >= 2.6
BuildRequires:  python-pkgconfig
BuildRequires:  python-six
BuildRequires:  python-sphinx
BuildRequires:  numpy >= 1.6.1
BuildRequires:  Cython
%if 0%{?with_python3}
BuildRequires:  python-tools
BuildRequires:  python%{python3_pkgversion}-devel >= 3.2
BuildRequires:  python%{python3_pkgversion}-pkgconfig
BuildRequires:  python%{python3_pkgversion}-six
BuildRequires:  python%{python3_pkgversion}-sphinx
BuildRequires:  python%{python3_pkgversion}-numpy >= 1.6.1
BuildRequires:  python%{python3_pkgversion}-Cython
%endif

%description
The h5py package provides both a high- and low-level interface to the
HDF5 library from Python. The low-level interface is intended to be a
complete wrapping of the HDF5 API, while the high-level component
supports access to HDF5 files, data sets and groups using established
Python and NumPy concepts.

A strong emphasis on automatic conversion between Python (Numpy)
data types and data structures and their HDF5 equivalents vastly
simplifies the process of reading and writing data from Python.

%package -n     python2-h5py
Summary:        A Python 2 interface to the HDF5 library
Group:          Applications/Engineering
Requires:       hdf5%{_isa} = %{_hdf5_version}
Requires:       numpy >= 1.6.1
Requires:       python2-six
%{?python_provide:%python_provide python2-h5py}
Obsoletes:      h5py < 2.6.0-1
Provides:       h5py = %{version}-%{release}

%description -n python2-h5py
The h5py package provides both a high- and low-level interface to the
HDF5 library from Python. The low-level interface is intended to be a
complete wrapping of the HDF5 API, while the high-level component
supports access to HDF5 files, data sets and groups using established
Python and NumPy concepts.

A strong emphasis on automatic conversion between Python (Numpy)
data types and data structures and their HDF5 equivalents vastly
simplifies the process of reading and writing data from Python.
This is the Python 2 version of h5py.

%if 0%{?with_python3}
%package -n     python%{python3_pkgversion}-h5py
Summary:        A Python %{python3_version} interface to the HDF5 library
Group:          Applications/Engineering
Requires:       hdf5%{_isa} = %{_hdf5_version}
Requires:       python%{python3_pkgversion}-numpy >= 1.6.1
Requires:       python%{python3_pkgversion}-six
%{?python_provide:%python_provide python%{python3_pkgversion}-h5py}

%description -n python%{python3_pkgversion}-h5py
The h5py package provides both a high- and low-level interface to the
HDF5 library from Python. The low-level interface is intended to be a
complete wrapping of the HDF5 API, while the high-level component
supports access to HDF5 files, data sets and groups using established
Python and NumPy concepts.

A strong emphasis on automatic conversion between Python (Numpy)
data types and data structures and their HDF5 equivalents vastly
simplifies the process of reading and writing data from Python.
This is the Python %{python3_version} version of h5py.
%endif

%prep
%setup -q
# use system libzlf and remove private copy
%patch0 -p1 -b .lzf
rm -rf lzf/lzf
%{__python2} api_gen.py
%if 0%{?with_python3}
rm -rf %{py3dir}
cp -a . %{py3dir}
%endif

%build
export CFLAGS="%{optflags} -fopenmp"
%py2_build

%if 0%{?with_python3}
pushd %{py3dir}
%py3_build
popd
%endif

%install
%py2_install

%if 0%{?with_python3}
pushd %{py3dir}
%py3_install
popd
%endif

%check
%{__python2} setup.py test
%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py test
popd
%endif

%files -n python2-h5py
%license licenses/*.txt
%doc ANN.rst README.rst examples
%{python_sitearch}/%{name}/
%{python_sitearch}/%{name}-%{version}-*.egg-info

%if 0%{?with_python3}
%files -n python%{python3_pkgversion}-h5py
%license licenses/*.txt
%doc ANN.rst README.rst
%{python3_sitearch}/%{name}/
%{python3_sitearch}/%{name}-%{version}-*.egg-info
%endif

%changelog
* Fri Nov 15 2019 Duncan Macleod <duncan.macleod@ligo.org> - 2.7.1-1
- Update to 2.7.1

* Wed Jun 25 2014 Orion Poplawski <orion@cora.nwra.com> - 2.3.1-1
- Update to 2.3.1

* Tue Jun 10 2014 Orion Poplawski <orion@cora.nwra.com> - 2.3.0-4
- Rebuild for hdf 1.8.13

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.3.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Fri May 9 2014 Orion Poplawski <orion@cora.nwra.com> - 2.3.0-2
- Rebuild for Python 3.4

* Tue Apr 22 2014 Orion Poplawski <orion@cora.nwra.com> - 2.3.0-1
- Update to 2.3.0

* Sun Jan 5 2014 Orion Poplawski <orion@cora.nwra.com> - 2.2.1-2
- Rebuild for hdf5 1.8.12
- Add requires for hdf5 version

* Thu Dec 19 2013 Orion Poplawski <orion@cora.nwra.com> - 2.2.1-1
- 2.2.1

* Thu Sep 26 2013 Terje Rosten <terje.rosten@ntnu.no> - 2.2.0-1
- 2.2.0

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.1.3-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Mon Jun 10 2013 Terje Rosten <terje.rosten@ntnu.no> - 2.1.3-1
- 2.1.3
- add Python 3 import patches (#962250)

* Thu May 16 2013 Orion Poplawski <orion@cora.nwra.com> - 2.1.0-3
- rebuild for hdf5 1.8.11

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.1.0-2
- rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Thu Dec 06 2012 Terje Rosten <terje.rosten@ntnu.no> - 2.1.0-1
- 2.1.0
- add Python 3 subpackage

* Thu Jul 19 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.0.1-2
- rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Tue Jan 24 2012 Terje Rosten <terje.rosten@ntnu.no> - 2.0.1-1
- 2.0.1
- docs is removed
- rebase patch

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.3.1-5
- rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Mon May 23 2011 Terje Rosten <terje.rosten@ntnu.no> - 1.3.1-4
- add patch from Steve Traylen (thanks!) to use system liblzf
 
* Thu Jan 13 2011 Terje Rosten <terje.rosten@ntnu.no> - 1.3.1-3
- fix buildroot
- add filter
- don't remove egg-info files
- remove explicit hdf5 req

* Sun Jan  2 2011 Terje Rosten <terje.rosten@ntnu.no> - 1.3.1-2
- build and ship docs as html

* Mon Dec 27 2010 Terje Rosten <terje.rosten@ntnu.no> - 1.3.1-1
- 1.3.1
- license is BSD only
- run tests
- new url

* Sat Jul  4 2009 Joseph Smidt <josephsmidt@gmail.com> - 1.2.0-1
- initial RPM release
